#pragma once

namespace dae
{
	enum class GameState : unsigned char
	{
		menu,
		playing,
		dead,
	};

	enum class ComponentID : unsigned char
	{
		Invalid,
		RenderComponent,
		FPSComponent,
		TransformComponent,
		TextComponent,
		InputComponent,
		PhysicsComponent,
		AIComponent,
		LivesComponent,
	};

	enum class ObjectTemplate
	{
		none = 0,
		pengo,
		snobee,
		wall,
	};

	enum class Event
	{
		livesChanged,
		pengoDead,
	};

#pragma warning(push)
#pragma warning(disable: 4100) //MSVC was complaining here about "msg" not being used. But we don't care as it's a base class.
	class BaseComponent
	{
	public:
		BaseComponent(const ComponentID& componentID) : mComponentID(componentID) {}
		virtual void Update(float deltaTime) {}
		virtual ~BaseComponent() {};
		virtual void RegisterComponent(const ComponentID& id, BaseComponent* component) {};
		const ComponentID& GetComponentID() { return mComponentID; }

	private:
		ComponentID mComponentID;
	};
#pragma warning(pop)
}