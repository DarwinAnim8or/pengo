#include "MiniginPCH.h"
#include "TransformComponent.h"

using namespace dae;

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::TransformComponent::Update(float deltaTime)
{
}

void dae::TransformComponent::RegisterComponent(BaseComponent * component)
{
}
void dae::TransformComponent::SetX(float x)
{
	mTransform.SetPosition(x, mTransform.GetPosition().y, 0.0f);
}
void dae::TransformComponent::SetY(float y)
{
	mTransform.SetPosition(mTransform.GetPosition().x, y, 0.0f);
}
float dae::TransformComponent::GetX()
{
	return mTransform.GetPosition().x;
}
float dae::TransformComponent::GetY()
{
	return mTransform.GetPosition().y;
}
#pragma warning(pop)

const dae::Transform & dae::TransformComponent::GetTransform()
{
	return mTransform;
}

void dae::TransformComponent::SetPosition(float x, float y)
{
	mTransform.SetPosition(x, y, 0.0f);
}
