#pragma once
#include "Common.h"
#include "TransformComponent.h"
#include "GameObject.h"

namespace dae
{
	class PhysicsComponent : public BaseComponent
	{
	public:
		PhysicsComponent(GameObject* obj) : parent(obj), BaseComponent(ComponentID::PhysicsComponent) {}
		~PhysicsComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);

		void SetVelocity(int vel) { mVelocity = vel; }
		void SetHorVelocity(int val) { mHorVelocity = val; }

		void CheckForCollisions();
		bool IsOverlapping(float ourX, float ourY, float x, float y);

	protected:
		GameObject* parent;
		TransformComponent* mTrans;
		int mVelocity;
		int mHorVelocity;

		const int mWidth = 32;
		const int mHeight = 32;
		bool mHorzOverlap = false;
		bool mVertOverlap = false;
	};
}