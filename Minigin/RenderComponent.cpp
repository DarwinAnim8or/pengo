#include "MiniginPCH.h"
#include "RenderComponent.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "Transform.h"
#include "TransformComponent.h"

using namespace dae;

void dae::RenderComponent::Render()
{
	if (!mTransformComponent || !mTexture) return;

	auto trans = mTransformComponent->GetTransform().GetPosition();
	Renderer::GetInstance().RenderTexture(*mTexture, trans.x, trans.y);
}

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::RenderComponent::Update(float deltaTime)
{
}
#pragma warning(pop)

void dae::RenderComponent::SetTexture(const std::string & path)
{
	mTexture = ResourceManager::GetInstance().LoadTexture(path);
}

void dae::RenderComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TransformComponent)
		mTransformComponent = static_cast<TransformComponent*>(component);
}
