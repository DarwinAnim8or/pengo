#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>

bool dae::InputManager::ProcessInput()
{
	ZeroMemory(&currentState, sizeof(XINPUT_STATE));
	XInputGetState(0, &currentState);
	mButtonsThisFrame.clear();

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
		if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym) 
			{
			case SDLK_ESCAPE: 
				return false;

			case SDLK_w:
				mButtonsThisFrame.push_back(ControllerButton::TrackUp);
				break;

			case SDLK_a:
				mButtonsThisFrame.push_back(ControllerButton::TrackLeft);
				break;

			case SDLK_s:
				mButtonsThisFrame.push_back(ControllerButton::TrackDown);
				break;

			case SDLK_d:
				mButtonsThisFrame.push_back(ControllerButton::TrackRight);
				break;

			case SDLK_h:
				mButtonsThisFrame.push_back(ControllerButton::ButtonA);
				break;

			case SDLK_j:
				mButtonsThisFrame.push_back(ControllerButton::ButtonB);
				break;

			case SDLK_k:
				mButtonsThisFrame.push_back(ControllerButton::ButtonX);
				break;

			case SDLK_l:
				mButtonsThisFrame.push_back(ControllerButton::ButtonY);
				break;
			}
		}
		if (e.type == SDL_MOUSEBUTTONDOWN) {
			
		}
	}

	return !IsPressed(ControllerButton::ButtonX);
}

bool dae::InputManager::IsPressed(ControllerButton button) const
{
	for (auto& b : mButtonsThisFrame)
		if (b == button) return true;

	switch (button)
	{
	case ControllerButton::ButtonA:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_A;
	case ControllerButton::ButtonB:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_B;
	case ControllerButton::ButtonX:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_X;
	case ControllerButton::ButtonY:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
	case ControllerButton::TrackUp:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
	case ControllerButton::TrackDown:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
	case ControllerButton::TrackLeft:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
	case ControllerButton::TrackRight:
		return currentState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;
	default: return false;
	}
}

