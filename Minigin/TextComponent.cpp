#include "MiniginPCH.h"
#include "TextComponent.h"
#include "Renderer.h"
#include "RenderComponent.h"
#include "ResourceManager.h"

void dae::TextComponent::Render()
{
}

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::TextComponent::Update(float deltaTime)
{
	if (mNeedsUpdate)
	{
		const SDL_Color color = { 255,255,255 }; // only white text is supported now
		const auto surf = TTF_RenderText_Blended(mFont->GetFont(), mText.c_str(), color);
		if (surf == nullptr)
		{
			throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
		}
		auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
		if (texture == nullptr)
		{
			throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
		}
		SDL_FreeSurface(surf);

		mRenderComponent->mTexture = std::make_shared<Texture2D>(texture);
		mNeedsUpdate = false;
	}
}
#pragma warning(pop)

void dae::TextComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::RenderComponent)
		mRenderComponent = static_cast<RenderComponent*>(component);
}

void dae::TextComponent::SetFont(const std::string & fontName, unsigned int size)
{
	mFont = ResourceManager::GetInstance().LoadFont(fontName, size);
}

void dae::TextComponent::SetText(const std::string & text)
{
	mText = text;
	mNeedsUpdate = true;
}
