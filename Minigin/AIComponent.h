#pragma once
#include "Common.h"
#include "GameObject.h"
#include "PhysicsComponent.h"
#include "TransformComponent.h"

namespace dae
{
	class AIComponent : public BaseComponent
	{
	public:
		AIComponent() : BaseComponent(ComponentID::AIComponent) {}
		~AIComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);
		void ResetPos();

	private:
		GameObject* mTarget;
		PhysicsComponent* mPhys;
		TransformComponent* mTrans;

		float startX;
		float startY;
	};
}