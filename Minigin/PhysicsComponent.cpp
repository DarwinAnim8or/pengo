#include "MiniginPCH.h"
#include "PhysicsComponent.h"
#include "SceneManager.h"
#include "GameObject.h"

dae::PhysicsComponent::~PhysicsComponent()
{
}

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::PhysicsComponent::Update(float deltaTime)
{
	if (!mTrans) return;

	int y = int(mTrans->GetY());
	y += mVelocity;
	mTrans->SetY(float(y));

	int x = int(mTrans->GetX());
	x += mHorVelocity;
	mTrans->SetX(float(x));

	CheckForCollisions();
}
#pragma warning(pop)

void dae::PhysicsComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TransformComponent)
		mTrans = static_cast<TransformComponent*>(component);
}

void dae::PhysicsComponent::CheckForCollisions()
{
	auto objects = SceneManager::GetInstance().GetAllGameObjects();
	for (auto o : objects)
	{
		if (o.second && o.second->GetComponent(ComponentID::PhysicsComponent)) {
			auto comp = static_cast<PhysicsComponent*>(o.second->GetComponent(ComponentID::PhysicsComponent));
			if (comp && comp != this)
			{
				float ourX = mTrans->GetX();
				float ourY = mTrans->GetY();

				float x = comp->mTrans->GetX();
				float y = comp->mTrans->GetY();

				if (IsOverlapping(ourX, ourY, x, y))
				{
					if (mHorzOverlap) {
						int fix = int(mTrans->GetX());
						fix -= mHorVelocity;
						mTrans->SetX(float(fix));
					}

					if (mVertOverlap) {
						int fix = int(mTrans->GetY());
						fix -= mVelocity;
						mTrans->SetY(float(fix));
					}

					mHorzOverlap = false;
					mVertOverlap = false;

					parent->OnHit(o.second);
				}
			}
		}
	}
}

bool dae::PhysicsComponent::IsOverlapping(float ourX, float ourY, float x, float y)
{
	// If one rectangle is on left side of the other
	if ((ourX + mWidth) < x || (x + mWidth) < ourX)
	{
		return false;
	}
	else {
		mHorzOverlap = true;
	}

	// If one rectangle is under the other
	if (ourY > (y + mHeight) || y > (ourY + mHeight))
	{
		return false;
	} 
	else {
		mVertOverlap = true;
	}

	return true;
}
