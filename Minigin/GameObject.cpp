#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "RenderComponent.h"
#include "SceneManager.h"
#include "FPSComponent.h"
#include "PhysicsComponent.h"
#include "AIComponent.h"
#include "InputComponent.h"
#include "LivesComponent.h"
#include "GameManager.h"
#include "Subject.h"

void dae::GameObject::SetPosition(float x, float y)
{
	if (mTrans) mTrans->SetPosition(x, y);
}

void dae::GameObject::OnHit(GameObject * other)
{
	if (other->GetTemplate() == ObjectTemplate::snobee && mTemplate == ObjectTemplate::pengo)
	{
		auto lives = static_cast<LivesComponent*>(GetComponent(ComponentID::LivesComponent));
		lives->SetLives(lives->GetLives() - 1);

		auto ai = static_cast<AIComponent*>(GetComponent(ComponentID::AIComponent));
		ai->ResetPos();
	}
}

dae::GameObject::GameObject(unsigned int objectID) :
	mObjectID(objectID)
{
}

dae::GameObject::~GameObject()
{
	for (auto& p : mComponents)
	{
		if (p.second)
		{
			delete p.second;
			p.second = nullptr;
		}
	}

	mComponents.clear();
}

void dae::GameObject::Update(float deltaTime)
{
	for (auto& p : mComponents)
	{
		if (p.second)
		{
			p.second->Update(deltaTime);
		}
	}
}

void dae::GameObject::Render() const
{
	for (auto& p : mComponents)
	{
		if (p.first == ComponentID::RenderComponent && p.second)
		{
			auto comp = static_cast<RenderComponent*>(p.second);
			if (comp) comp->Render();
		}
	}
}

const unsigned int dae::GameObject::GetObjectID()
{
	return mObjectID;
}

void dae::GameObject::AddComponent(BaseComponent * component)
{
	mComponents.insert(std::make_pair(component->GetComponentID(), component));
}

dae::BaseComponent * dae::GameObject::GetComponent(ComponentID id)
{
	for (auto comp : mComponents)
		if (comp.first == id) return comp.second;

	return nullptr;
}

void dae::GameObject::ApplyTemplate(ObjectTemplate temp)
{
	mTemplate = temp;

	switch (temp)
	{
	case ObjectTemplate::pengo: {
		auto ren = new RenderComponent();
		auto trans = new TransformComponent();
		auto input = new InputComponent();
		auto phys = new PhysicsComponent(this);
		auto lives = new LivesComponent(mObjectID);

		AddComponent(ren);
		AddComponent(trans);
		AddComponent(input);
		AddComponent(phys);
		AddComponent(lives);
		phys->RegisterComponent(trans);
		input->RegisterComponent(phys);
		ren->RegisterComponent(trans);
		ren->SetTexture("pengo.png");
		mTrans = trans;
		break;
	}

	case ObjectTemplate::wall: {
		auto ren = new RenderComponent();
		auto trans = new TransformComponent();
		auto phys = new PhysicsComponent(this);
		AddComponent(ren);
		AddComponent(trans);
		AddComponent(phys);
		phys->RegisterComponent(trans);
		ren->RegisterComponent(trans);
		ren->SetTexture("no.png");
		mTrans = trans;
		break;
	}

	case ObjectTemplate::snobee: {
		auto ren = new RenderComponent();
		auto trans = new TransformComponent();
		auto phys = new PhysicsComponent(this);
		auto ai = new AIComponent();
		AddComponent(ren);
		AddComponent(trans);
		AddComponent(phys);
		AddComponent(ai);
		phys->RegisterComponent(trans);
		ren->RegisterComponent(trans);
		ren->SetTexture("snobee.png");
		ai->RegisterComponent(trans);
		ai->RegisterComponent(phys);
		mTrans = trans;
		break;
	}
	}
}
