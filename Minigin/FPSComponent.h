#pragma once
#include "Common.h"

namespace dae
{
	class TextComponent;

	class FPSComponent : public BaseComponent
	{
	public:
		FPSComponent() : BaseComponent(ComponentID::FPSComponent) {}
		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);

	private:
		int mFPS;
		TextComponent* mTextComponent;
	};
}