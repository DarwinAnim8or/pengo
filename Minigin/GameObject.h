#pragma once
#include "Common.h"
#include "SceneObject.h"
#include <map>
#include "TransformComponent.h"

class BaseComponent;
class BaseMessage;

namespace dae
{
	class GameObject final : public SceneObject
	{
	public:
		void Update(float deltaTime) override;
		void Render() const override;

		const unsigned int GetObjectID();
		void AddComponent(BaseComponent* component);
		BaseComponent* GetComponent(ComponentID id);

		void ApplyTemplate(ObjectTemplate temp);
		const ObjectTemplate& GetTemplate() { return mTemplate; }

		void SetPosition(float x, float y);
		void OnHit(GameObject* other);

		GameObject(unsigned int objectID);
		~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	private:
		unsigned int mObjectID;
		ObjectTemplate mTemplate;
		std::map<ComponentID, BaseComponent*> mComponents;
		TransformComponent* mTrans;
	};
}
