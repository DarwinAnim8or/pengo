#include "MiniginPCH.h"
#include "InputComponent.h"
#include "InputManager.h"
#include <vector>

dae::InputComponent::~InputComponent()
{
}

void dae::InputComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::PhysicsComponent)
		mPhys = static_cast<PhysicsComponent*>(component);
}

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::InputComponent::Update(float deltaTime)
{
	if (!mPhys) return;
	auto& instance = InputManager::GetInstance();

	if (instance.IsPressed(ControllerButton::TrackUp)) {
		mPhys->SetVelocity(-accel);
	}
	else if (instance.IsPressed(ControllerButton::TrackDown)) {
		mPhys->SetVelocity(accel);
	}
	else {
		mPhys->SetVelocity(0);
	}

	if (instance.IsPressed(ControllerButton::TrackLeft)) {
		mPhys->SetHorVelocity(-accel);
	}
	else if (instance.IsPressed(ControllerButton::TrackRight)) {
		mPhys->SetHorVelocity(accel);
	}
	else {
		mPhys->SetHorVelocity(0);
	}
}
#pragma warning(pop)