#pragma once
#include "Common.h"
#include "PhysicsComponent.h"

namespace dae
{
	class InputComponent : public BaseComponent
	{
	public:
		InputComponent() : BaseComponent(ComponentID::InputComponent) {}
		~InputComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);

	private:
		static const int accel = 3;
		PhysicsComponent* mPhys;
	};
}


