#include "MiniginPCH.h"
#include "GameManager.h"
#include "ResourceManager.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "BinaryReader.h"
#include "InputManager.h"

#include "TextComponent.h"
#include "RenderComponent.h"
#include "FPSComponent.h"
#include "TransformComponent.h"
#include "InputComponent.h"
#include "PhysicsComponent.h"
#include "Logger.h"

using namespace dae;

void dae::GameManager::Initialize()
{
	mGameState = GameState::menu;
	mHasCompletedLevelOne = false;
}

void dae::GameManager::LoadMainMenu()
{
	//Create our title:
	auto go = SceneManager::GetInstance().CreateObject();
	auto text = new TextComponent();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 36);
	trans->SetPosition(250.0f, 20.0f);
	text->SetText("Pengo");

	//Create our text objects:
	go = SceneManager::GetInstance().CreateObject();
	text = new TextComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);
	trans->SetPosition(20.0f, 100.0f);
	text->SetText("Press A (h) to start with one player.");

	go = SceneManager::GetInstance().CreateObject();
	text = new TextComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);
	trans->SetPosition(20.0f, 140.0f);
	text->SetText("Press X (k) to quit (or Y (l) to reset while playing)");
}

void dae::GameManager::LoadLevel()
{
	SceneManager::GetInstance().DestroyAllObjects();

	//auto reader = BinaryReader("./res/level1.lvl");
	LoadBaseLevel();
	LoadFirstLevel();

	//if (mHasCompletedLevelOne) LoadFirstLevel();
	//else LoadSecondLevel();

	//Create Pengo:
	auto player = SceneManager::GetInstance().CreateObject();
	player->ApplyTemplate(ObjectTemplate::pengo);
	player->SetPosition(320.0f, 320.0f);

	//Create our FPS object:
	auto go = SceneManager::GetInstance().CreateObject();
	auto fps = new FPSComponent();
	auto text = new TextComponent();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();

	go->AddComponent(fps);
	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	fps->RegisterComponent(text);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);

	////Create our health display object:
	//go = SceneManager::GetInstance().CreateObject();
	//auto lives = new LivesDisplayComponent();
	//ren = new RenderComponent();
	//trans = new TransformComponent();
	//text = new TextComponent();

	//go->AddComponent(lives);
	//go->AddComponent(text);
	//go->AddComponent(ren);
	//go->AddComponent(trans);

	//text->RegisterComponent(ren);
	//lives->RegisterComponent(text);
	//ren->RegisterComponent(trans);
	//text->SetFont("Lingua.otf", 24);
	//trans->SetPosition(280.0f, 0.0f);

	//player->addObserver(lives);
	//player->OnHit();

	mGameState = GameState::playing;
	pCommon::Logger::GetInstance().Log("GameManager", "GAME START");
}

void dae::GameManager::CheckInput()
{
	if (InputManager::GetInstance().IsPressed(ControllerButton::ButtonA) && mGameState == GameState::menu)
		LoadLevel();
	else if (InputManager::GetInstance().IsPressed(ControllerButton::ButtonY) && mGameState == GameState::playing)
		LoadLevel();
}

void dae::GameManager::onNotify(unsigned int objectID, Event event)
{
	switch (event)
	{
	case Event::pengoDead: {
		mGameState = GameState::dead;
		pCommon::Logger::GetInstance().Log("GameManager", "GAME OVER");
		break;
	}

	case Event::livesChanged: {
		pCommon::Logger::GetInstance().Log("GameManager", std::to_string(objectID) + " lost a life");
		break;
	}
	}
}

void dae::GameManager::LoadBaseLevel()
{
	float y = 100.0f;
	for (int i = 0; i < 16; ++i)
	{
		auto go = SceneManager::GetInstance().CreateObject();
		go->ApplyTemplate(ObjectTemplate::wall);
		go->SetPosition(20.0f, y + (32.0f * float(i)));
	}

	y = 100.0f;
	for (int i = 0; i < 16; ++i)
	{
		auto go = SceneManager::GetInstance().CreateObject();
		go->ApplyTemplate(ObjectTemplate::wall);
		go->SetPosition(600.0f, y + (32.0f * float(i)));
	}

	float x = 36.0f;
	for (int i = 0; i < 18; ++i)
	{
		auto go = SceneManager::GetInstance().CreateObject();
		go->ApplyTemplate(ObjectTemplate::wall);
		go->SetPosition(x + (32.0f * float(i)), 100.0f);
	}

	x = 36.0f;
	for (int i = 0; i < 18; ++i)
	{
		auto go = SceneManager::GetInstance().CreateObject();
		go->ApplyTemplate(ObjectTemplate::wall);
		go->SetPosition(x + (32.0f * float(i)), 580.0f);
	}
}

void dae::GameManager::LoadFirstLevel()
{
	//Create our test enemies:
	auto go = SceneManager::GetInstance().CreateObject();
	go->ApplyTemplate(ObjectTemplate::snobee);
	go->SetPosition(150.0f, 150.0f);

	go = SceneManager::GetInstance().CreateObject();
	go->ApplyTemplate(ObjectTemplate::snobee);
	go->SetPosition(250.0f, 150.0f);

	go = SceneManager::GetInstance().CreateObject();
	go->ApplyTemplate(ObjectTemplate::snobee);
	go->SetPosition(250.0f, 400.0f);
}

void dae::GameManager::LoadSecondLevel()
{
}
