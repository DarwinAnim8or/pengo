#pragma once
#include "Common.h"
#include "Subject.h"
#include "GameManager.h"
#include "Logger.h"

namespace dae
{
	class LivesComponent : public BaseComponent, Subject
	{
	public:
		LivesComponent(unsigned int objID) : objectID(objID), BaseComponent(ComponentID::LivesComponent) {
			
		}
		~LivesComponent();

		void SetLives(int lives) { 
			mLives = lives; 
			notify(objectID, Event::livesChanged); 
			pCommon::Logger::GetInstance().Log("LivesComponent", "We lost a life!");

			if (mLives == 0) notify(objectID, Event::pengoDead);
		}

		const int GetLives() const { return mLives; }

	private:
		unsigned int objectID;
		int mLives = 3;
	};
}