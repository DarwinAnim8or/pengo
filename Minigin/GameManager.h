#pragma once
#include <string>
#include "Singleton.h"
#include "Common.h"
#include "Observer.h"

namespace dae
{
	class GameManager final : public pCommon::Singleton<GameManager>, Observer
	{
	public:
		void Initialize();

		void LoadMainMenu();
		void LoadLevel();

		void CheckInput();

		const GameState& GetGameState() const { return mGameState; }

		void onNotify(unsigned int objectID, Event event);

	private:
		void LoadBaseLevel();
		void LoadFirstLevel();
		void LoadSecondLevel();

	private:
		GameState mGameState;
		bool mHasCompletedLevelOne = false;
	};
}