#include "MiniginPCH.h"
#include "AIComponent.h"
#include "SceneManager.h"
#include <future>
#include "LivesComponent.h"

dae::AIComponent::~AIComponent()
{
}

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::AIComponent::Update(float deltaTime)
{
	std::async(std::launch::async, [&]() {
		if (!mTarget)
		{
			auto objects = SceneManager::GetInstance().GetAllGameObjects();
			for (auto obj : objects)
			{
				if (obj.second && obj.second->GetTemplate() == ObjectTemplate::pengo)
				{
					mTarget = obj.second;
					break;
				}
			}
		}
		else
		{
			auto tr = static_cast<TransformComponent*>(mTarget->GetComponent(ComponentID::TransformComponent));

			if (int(tr->GetX()) == int(mTrans->GetX()))
			{
				mPhys->SetHorVelocity(0);
			}

			if (int(tr->GetY()) == int(mTrans->GetY())) {
				mPhys->SetVelocity(0);
			}

			if (int(tr->GetX()) > int(mTrans->GetX()))
				mPhys->SetHorVelocity(1);

			if (int(tr->GetX()) < int(mTrans->GetX()))
				mPhys->SetHorVelocity(-1);

			if (int(tr->GetY()) < int(mTrans->GetY()))
				mPhys->SetVelocity(-1);

			if (int(tr->GetY()) > int(mTrans->GetY()))
				mPhys->SetVelocity(1);
		}
	});
}
#pragma warning(pop)

void dae::AIComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::PhysicsComponent)
		mPhys = static_cast<PhysicsComponent*>(component);

	if (component->GetComponentID() == ComponentID::TransformComponent) {
		mTrans = static_cast<TransformComponent*>(component);
		startX = mTrans->GetX();
		startY = mTrans->GetY();
	}
}

void dae::AIComponent::ResetPos()
{
	mTrans->SetPosition(startX, startY);
}
