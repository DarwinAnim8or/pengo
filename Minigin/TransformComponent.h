#pragma once
#include "Common.h"
#include "Transform.h"

namespace dae
{
	class TransformComponent : public BaseComponent
	{
	public:
		TransformComponent() : BaseComponent(ComponentID::TransformComponent) {}
		void Update(float deltaTime);
		void SetPosition(float x, float y);
		void RegisterComponent(BaseComponent* component);

		void SetX(float x);
		void SetY(float y);

		float GetX();
		float GetY();

		const Transform& GetTransform();

	private:
		Transform mTransform;
	};
}