#pragma once
#include "Singleton.h"
#include <map>
#include <vector>

namespace dae
{
	class GameObject;

	class SceneManager final : public pCommon::Singleton<SceneManager>
	{
	public:

		void Update(float deltaTime);
		void Render();

		GameObject* CreateObject();

		~SceneManager();
		void DestroyAllObjects();
		void DestroyObject(unsigned int objectID);

		GameObject* GetGameObject(unsigned int objectID);
		std::map<unsigned int, GameObject*>& GetAllGameObjects() { return mObjects; }

	private:
		static unsigned int mNextObjectID;
		std::map<unsigned int, GameObject*> mObjects;
		std::vector<unsigned int> mObjectsMarkedForDelete;
	};
}
