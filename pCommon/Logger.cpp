#include "Logger.h"

#include <Windows.h>
#include <time.h>

using namespace pCommon;

void Logger::Init(const std::string& filename)
{
	if (!dirExists("./logs")) CreateDirectory("./logs", NULL);
	
	m_File = std::ofstream("./logs/" + filename);
	if (!m_File.is_open()) {
		std::cout << "Couldn't open " << filename << " for writing!" << std::endl;
		return;
	}

	//Get the current directory:
	std::string toLog = "Starting \"Pengo\" in directory: ";
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	toLog.append(std::string(buffer).substr(0, pos));

	Log("Logger", toLog);
}

Logger::~Logger()
{
	m_File.close();
}

void pCommon::Logger::Log(const std::string & className, const std::string & message)
{
	if (!m_File.is_open()) return;

	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	localtime_s(&tstruct, &now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	m_File << std::string(buf) << "\t[" << className << "]: " << message << std::endl;
}

bool pCommon::Logger::dirExists(const std::string & dirName)
{
	DWORD ftyp = GetFileAttributesA(dirName.c_str());

	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}
