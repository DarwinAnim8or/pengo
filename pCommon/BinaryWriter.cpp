#include "BinaryWriter.h"
#include <iostream>

using namespace pCommon;

BinaryWriter::BinaryWriter(const std::string& filepath) : m_Outpath{ filepath }
{
	m_Outstream = new std::ofstream();
	m_Outstream->open(filepath, std::ios::out | std::ios::binary);
	if (m_Outstream->is_open()) {
		std::cout << "Opened file: " << filepath << " for writing.\n";
	}
}

BinaryWriter::~BinaryWriter()
{
	if (m_Outstream && m_Outstream->is_open()) {
		std::cout << "Closing file: " << m_Outpath << ".\n";
		m_Outstream->close();
		delete m_Outstream;
		m_Outstream = nullptr;
	}
}

void BinaryWriter::WriteString(const std::string & str)
{
	WriteVariable(unsigned char(str.length()));

	for (unsigned char i = 0; i < unsigned char(str.length()); ++i) {
		WriteVariable(str[i]);
	}
}

const bool BinaryWriter::GetIsOkay()
{
	if (!m_Outstream) return false;
	return m_Outstream->is_open();
}

const size_t BinaryWriter::GetCurrentPosition()
{
	if (!m_Outstream) return size_t();
	return size_t(m_Outstream->tellp());
}

void BinaryWriter::Seek(size_t newPosition)
{
	if (!m_Outstream) return;
	m_Outstream->seekp(newPosition);
}
