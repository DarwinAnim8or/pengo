#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "Singleton.h"

namespace pCommon {
	class Logger : public Singleton<Logger>
	{
	public:
		void Init(const std::string& filename);
		~Logger();

		void Log(const std::string& className, const std::string& message);

	private:
		std::ofstream m_File;

	private:
		//Taken from StackOverflow:
		//http://r.duckduckgo.com/l/?kh=-1&uddg=http%3A%2F%2Fstackoverflow.com%2Fquestions%2F8233842%2Fddg%238233867
		bool dirExists(const std::string& dirName);
	};
}